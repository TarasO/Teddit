package com.example.teddit.repository;

import com.example.teddit.model.Post;
import com.example.teddit.model.User;
import com.example.teddit.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopByPostAndUserOrderByIdDesc(Post post, User currentUser);
}