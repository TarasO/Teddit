package com.example.teddit.repository;

import com.example.teddit.model.Post;
import com.example.teddit.model.Subreddit;
import com.example.teddit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findBySubreddit(Subreddit subreddit);
    List<Post> findByUser(User user);
}