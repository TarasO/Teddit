package com.example.teddit.repository;

import com.example.teddit.model.Comment;
import com.example.teddit.model.Post;
import com.example.teddit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByPost(Post post);
    List<Comment> findByUser(User user);
}