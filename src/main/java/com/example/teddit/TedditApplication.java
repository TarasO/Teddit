package com.example.teddit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class TedditApplication {

    public static void main(String[] args) {
        SpringApplication.run(TedditApplication.class, args);
    }

}
