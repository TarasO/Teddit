package com.example.teddit.controller;

import com.example.teddit.dto.SubredditDto;
import com.example.teddit.service.SubredditService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/subreddits")
@AllArgsConstructor
@Slf4j
public class SubredditController {

    private final SubredditService subredditService;

    @PostMapping
    public ResponseEntity<Object> createSubreddit(@RequestBody SubredditDto subredditDto) {
        Long id = subredditService.save(subredditDto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping
    public ResponseEntity<List<SubredditDto>> getSubreddits() {
        return ResponseEntity.ok(subredditService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<SubredditDto> getSubreddit(@PathVariable("id") Long id) {
        return ResponseEntity.ok(subredditService.findSubredditById(id));
    }

}
