package com.example.teddit.controller;

import com.example.teddit.dto.CommentDto;
import com.example.teddit.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping
    public ResponseEntity<CommentDto> createComment(@RequestBody CommentDto comment) {
        Long id = commentService.save(comment);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/by-post/{id}")
    public ResponseEntity<List<CommentDto>> getCommentsByPost(@PathVariable("id") Long postId) {
        return ResponseEntity.ok(commentService.findAllByPostId(postId));
    }

    @GetMapping("/by-user/{id}")
    public ResponseEntity<List<CommentDto>> getCommentsByUser(@PathVariable("id") Long userId) {
        return ResponseEntity.ok(commentService.findAllByUserId(userId));
    }
}
