package com.example.teddit.service;

import com.example.teddit.dto.VoteDto;
import com.example.teddit.exceptions.NotFoundException;
import com.example.teddit.exceptions.SpringRedditException;
import com.example.teddit.mapper.VoteMapper;
import com.example.teddit.model.Post;
import com.example.teddit.model.Vote;
import com.example.teddit.repository.PostRepository;
import com.example.teddit.repository.VoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.teddit.model.VoteType.UPVOTE;

@Service
@AllArgsConstructor
public class VoteService {

    private final VoteRepository voteRepository;
    private final PostRepository postRepository;
    private final AuthService authService;
    private final VoteMapper voteMapper;

    @Transactional
    public Long vote(VoteDto voteDto) {
        Post post = postRepository.findById(voteDto.getPostId())
                .orElseThrow(() -> new NotFoundException("Post with id "
                                                         + voteDto.getPostId()
                                                         + " is not found"));
        voteRepository.findTopByPostAndUserOrderByIdDesc(post, authService.getCurrentUser())
                .map(Vote::getVoteType)
                .ifPresent(voteType -> {
                    if(voteType.equals(voteDto.getVoteType())) {
                        throw new SpringRedditException("You have already "
                                                        + voteDto.getVoteType()
                                                        + "'d for this post");
                    }
                });

        if(voteDto.getVoteType().equals(UPVOTE)) {
            post.setVoteCount(post.getVoteCount() + 1);
        } else {
            post.setVoteCount(post.getVoteCount() - 1);
        }

        Vote vote = voteMapper.mapDtoToVote(voteDto);
        voteRepository.save(vote);
        postRepository.save(post);

        return vote.getId();
    }
}
