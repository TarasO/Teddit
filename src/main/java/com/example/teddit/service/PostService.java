package com.example.teddit.service;

import com.example.teddit.dto.PostRequest;
import com.example.teddit.dto.PostResponse;
import com.example.teddit.exceptions.NotFoundException;
import com.example.teddit.mapper.PostMapper;
import com.example.teddit.model.Post;
import com.example.teddit.model.Subreddit;
import com.example.teddit.repository.PostRepository;
import com.example.teddit.repository.SubredditRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class PostService {

    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final SubredditRepository subredditRepository;
    private final AuthService authService;

    public Long save(PostRequest postRequest) {
        postRequest.setCreatedAt(LocalDateTime.now());
        Post post = postMapper.mapDtoToPost(postRequest);
        post.setUser(authService.getCurrentUser());
        postRepository.save(post);
        return post.getId();
    }

    @Transactional(readOnly = true)
    public List<PostResponse> findAll() {
        return postRepository.findAll()
                .stream()
                .map(postMapper::mapPostToDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public PostResponse findPostById(Long id) {
        return postRepository.findById(id)
                .map(postMapper::mapPostToDto)
                .orElseThrow(() -> new NotFoundException("Post with id " + id + " is not found"));
    }

    @Transactional(readOnly = true)
    public List<PostResponse> findAllBySubreddit(Long subredditId) {
        Subreddit subreddit = subredditRepository.findById(subredditId)
                .orElseThrow(() -> new NotFoundException("Subreddit with id " + subredditId + "is not found"));
        return postRepository.findBySubreddit(subreddit)
                .stream()
                .map(postMapper::mapPostToDto)
                .collect(Collectors.toList());
    }
}
