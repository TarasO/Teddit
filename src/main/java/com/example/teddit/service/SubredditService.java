package com.example.teddit.service;

import com.example.teddit.dto.SubredditDto;
import com.example.teddit.exceptions.NotFoundException;
import com.example.teddit.mapper.SubredditMapper;
import com.example.teddit.model.Subreddit;
import com.example.teddit.repository.SubredditRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class SubredditService {

    private final SubredditRepository subredditRepository;
    private final SubredditMapper subredditMapper;

    @Transactional
    public Long save(SubredditDto subredditDto) {
        subredditDto.setCreatedAt(LocalDateTime.now());
        Subreddit subreddit = subredditMapper.mapDtoToSubreddit(subredditDto);
        subredditRepository.save(subreddit);
        return subreddit.getId();
    }

    @Transactional(readOnly = true)
    public List<SubredditDto> findAll() {
        return subredditRepository.findAll()
                .stream()
                .map(subredditMapper::mapSubredditToDto)
                .collect(Collectors.toList());
    }

    public SubredditDto findSubredditById(Long id) {
        return subredditRepository.findById(id)
                .map(subredditMapper::mapSubredditToDto)
                .orElseThrow(() -> new NotFoundException("Subreddit with id " + id + " is not found"));
    }
}
