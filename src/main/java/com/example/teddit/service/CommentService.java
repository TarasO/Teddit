package com.example.teddit.service;

import com.example.teddit.dto.CommentDto;
import com.example.teddit.exceptions.NotFoundException;
import com.example.teddit.mapper.CommentMapper;
import com.example.teddit.model.Comment;
import com.example.teddit.model.Post;
import com.example.teddit.model.User;
import com.example.teddit.repository.CommentRepository;
import com.example.teddit.repository.PostRepository;
import com.example.teddit.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final AuthService authService;

    public Long save(CommentDto commentDto) {
        commentDto.setCreatedAt(LocalDateTime.now());
        Comment comment = commentMapper.mapDtoToComment(commentDto);
        comment.setUser(authService.getCurrentUser());
        commentRepository.save(comment);
        return comment.getId();
    }

    public List<CommentDto> findAllByPostId(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new NotFoundException("Post with id" + postId + " is not found"));
        return commentRepository.findByPost(post)
                .stream().map(commentMapper::mapCommentToDto)
                .collect(Collectors.toList());
    }

    public List<CommentDto> findAllByUserId(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User with id" + userId + " is not found"));
        return commentRepository.findByUser(user)
                .stream().map(commentMapper::mapCommentToDto)
                .collect(Collectors.toList());
    }
}
