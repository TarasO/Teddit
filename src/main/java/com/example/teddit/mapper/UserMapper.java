package com.example.teddit.mapper;

import com.example.teddit.dto.UserDto;
import com.example.teddit.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto mapUserToDto(User user);
}
