package com.example.teddit.mapper;

import com.example.teddit.dto.VoteDto;
import com.example.teddit.model.Vote;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VoteMapper {
    Vote mapDtoToVote(VoteDto voteDto);
}
