package com.example.teddit.mapper;

import com.example.teddit.dto.PostRequest;
import com.example.teddit.dto.PostResponse;
import com.example.teddit.model.Post;
import com.example.teddit.model.VoteType;
import com.example.teddit.repository.CommentRepository;
import com.example.teddit.repository.VoteRepository;
import com.example.teddit.service.AuthService;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.github.marlonlom.utilities.timeago.TimeAgoMessages;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZoneOffset;

import static com.example.teddit.model.VoteType.DOWNVOTE;
import static com.example.teddit.model.VoteType.UPVOTE;

@Mapper(componentModel = "spring", uses = {UserMapper.class, SubredditMapper.class})
public abstract class PostMapper {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private AuthService authService;

    @Mapping(target = "subreddit", expression = "java(com.example.teddit.model.Subreddit.builder().id(postRequest.getSubredditId()).build())")
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "voteCount", constant = "0")
    public abstract Post mapDtoToPost(PostRequest postRequest);

    @Mapping(target = "commentCount", expression = "java(commentCount(post))")
    @Mapping(target = "duration", expression = "java(getDuration(post))")
    @Mapping(target = "upVote", expression = "java(isPostUpVoted(post))")
    @Mapping(target = "downVote", expression = "java(isPostDownVoted(post))")
    public abstract PostResponse mapPostToDto(Post post);

    protected Integer commentCount(Post post) {
        return commentRepository.findByPost(post).size();
    }

    protected String getDuration(Post post) {
        return TimeAgo.using(post.getCreatedAt().toInstant(ZoneOffset.UTC).toEpochMilli());
    }

    protected boolean isPostUpVoted(Post post) {
        return checkVoteType(post, UPVOTE);
    }

    protected boolean isPostDownVoted(Post post) {
        return checkVoteType(post, DOWNVOTE);
    }

    private boolean checkVoteType(Post post, VoteType voteType) {
        if (authService.isLoggedIn()) {
            return voteRepository.findTopByPostAndUserOrderByIdDesc(post, authService.getCurrentUser())
                    .filter(vote -> vote.getVoteType().equals(voteType))
                    .isPresent();
        }
        return false;
    }
}
