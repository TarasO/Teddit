package com.example.teddit.mapper;

import com.example.teddit.dto.CommentDto;
import com.example.teddit.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {UserMapper.class, PostMapper.class})
public interface CommentMapper {

    @Mapping(target = "post", expression = "java(com.example.teddit.model.Post.builder().id(commentDto.getPostId()).build())")
    @Mapping(target = "user", ignore = true)
    Comment mapDtoToComment(CommentDto commentDto);

    @Mapping(target = "postId", expression = "java(comment.getPost().getId())")
    CommentDto mapCommentToDto(Comment comment);
}
